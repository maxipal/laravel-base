<?php

namespace Stylemix\Base\Fields;

use Illuminate\Support\Arr;

class CheckboxField extends Base
{

	public $component = 'checkbox-field';

	protected $typeRules = [
		'boolean'
	];

	/**
	 * @inheritdoc
	 */
	protected function sanitizeRequestInput($value)
	{
		return boolval($value);
	}

	/**
	 * Resolve the field's value.
	 *
	 * @param  mixed       $data
	 * @param  string|null $attribute
	 *
	 * @return mixed
	 */
	public function resolve($data, $attribute = null)
	{
		$value = null;

		// It make no sense to resolve empty resource
		// It should be checked for null or empty array
		if (!empty($data)) {
			$attribute = $attribute ?? $this->attribute;
			$value = $this->resolveAttribute($data, $attribute);

			if (is_callable($this->resolveCallback)) {
				$value = call_user_func(
					$this->resolveCallback, $value, $data, $this
				);
			}

			$value = $this->multiple ?
				array_map([$this, 'sanitizeResolvedValue'], Arr::wrap($value)) :
				// We shouldn't sanitize null value, since it means no value
				(is_null($value) ? null : $this->sanitizeResolvedValue($value));
		}

		// Provide empty array in case the field is multiple
		if (!$value && $this->multiple) {
			$value = [];
		}
		return boolval($value);
	}

}
