<?php

namespace Stylemix\Base\QueryBuilder;

use Illuminate\Support\Carbon;

class DatetimeFilter extends NumberFilter
{

	protected function parseValue($value)
	{
		return Carbon::parse($value)
			->setTimezone(config('app.timezone'))
			->toDateTimeString();
	}

}
